use std::collections::HashSet;
use std::fs;

fn main() {
    let str = fs::read_to_string("input.txt").unwrap();
    let mut result = 14;
    let mut set = HashSet::new();
    for i in 0..str.len() {
        for j in 0..14 {
            set.insert(str.as_bytes()[i + j] as char);
        }
        if set.len() == 14 {
            println!("{}", result);
            return;
        }
        else {
            set.remove(&(str.as_bytes()[i] as char));
            result += 1;
        }
    }
}
